import React, { Component } from 'react';
import './Game.css';
import {Coords, getDestCoords, increaseScore} from "./Utils";
import Player from './Player'
import Dice from './Dice'
import RemoteDice from './RemoteDice'
import playerImage from './player.png'
import enemeyImage from './enemy.png'
import {BoardHeight} from "./Consts";


class Game extends Component {

    constructor(props) {
        super(props);
        this.moveFinished = this.moveFinished.bind(this);
        this.moveStarted = this.moveStarted.bind(this);
        this.state = {
            playerCoords: this.props.playerCoords,
            enemyCoords: this.props.enemyCoords,
            moving: false,
            ourTurn: this.props.ourTurn,
        };
        this.moveCounter = 0
    }

    jump(coords) {
        const jumpPosition = {
            3: 21,
            8: 30,
            17: 13,
            28: 84,
            52: 29,
            57: 40,
            58: 77,
            62: 22,
            75: 86,
            80: 100,
            88: 18,
            90: 91,
            95: 51,
            97: 79
        };
        let start = this.CoordsToNum(coords);
        let result = start;
        if (start + 1 in jumpPosition)
            result = jumpPosition[start + 1] - 1;
        return this.NumToCoords(result);
    }

    NumToCoords(num) {
        let x = num % 10;
        const y = (num - x) / 10;

        if (y % 2 === 1)
            x = 9 - x;

        return new Coords(x, y);
    }

    CoordsToNum(coords) {
        if (coords.Y % 2 === 0)
            return coords.Y * 10 + coords.X;
        else
            return (coords.Y + 1) * 10 - coords.X - 1;
    }

    movePlayer() {
        if (this.moveCounter === 0) {
            console.warning("Tried moving player without moves");
            return;
        }
        this.moveCounter--;
        let coords = this.state.ourTurn ?
            this.state.playerCoords : this.state.enemyCoords;
        let newCoords = getDestCoords(coords, 1);

        this.setState(this.state.ourTurn ? {
            playerCoords: newCoords,
        } : {
            enemyCoords: newCoords,
        });
        setTimeout(() => this.moveFinished(), 400);
    }
    moveFinished() {
        if (!this.state.moving)
            return;
        if (this.moveCounter === 0) {
            const ourTurn = this.state.ourTurn;
            const newCoords = ourTurn ? {
                playerCoords: this.jump(this.state.playerCoords)
            } : {
                enemyCoords: this.jump(this.state.enemyCoords)
            };
            this.setState(Object.assign({
                moving: false,
                ourTurn: !ourTurn,
            }, newCoords));
            const playerWin = (
                this.state.playerCoords.X === 0 &&
                this.state.playerCoords.Y === BoardHeight - 1
            );
            const enemyWin = (
                this.state.enemyCoords.X === 0 &&
                this.state.enemyCoords.Y === BoardHeight - 1
            );
            if (playerWin)
                increaseScore(3);
            else if (enemyWin)
                increaseScore(1);
        }
        else
            this.movePlayer();
    }

    moveStarted(num, ourPlayer) {
        if (this.state.ourTurn !== ourPlayer)
            return;
        this.setState({
            moving: true
        });
        this.moveCounter = num;
        this.movePlayer();
    }



    render() {
        const playerWin = (
            this.state.playerCoords.X === 0 &&
            this.state.playerCoords.Y === BoardHeight - 1
        );
        const enemyWin = (
            this.state.enemyCoords.X === 0 &&
            this.state.enemyCoords.Y === BoardHeight - 1
        );
        const finished = enemyWin || playerWin;
        const shared = (
            this.state.playerCoords.X === this.state.enemyCoords.X &&
            this.state.playerCoords.Y === this.state.enemyCoords.Y
        );
        const playerStop = () => this.moveFinished(true);
        const enemyStop = () => this.moveFinished(false);
        const playerDiced = (num) => this.moveStarted(num, true);
        const enemyDiced = (num) => this.moveStarted(num, false);
        return (

            <div className="game">
                <div className="boardWrapper">
                    <div className="board" style={{
                        opacity: (finished ? 0.5 : 1)
                    }}>
                        <Player coords={this.state.playerCoords}
                                    onStop={playerStop}
                                icon={playerImage}
                                leftAligned={shared}

                        />
                        <Player coords={this.state.enemyCoords}
                                onStop={enemyStop}
                                icon={enemeyImage}
                                rightAligned={shared}
                        />

                    </div>
                    {finished && (
                        <div className={"finishedText " + (playerWin ? "winnerText": "loserText")}>
                            {playerWin ? "You Win >:)": "You lose :("}
                        </div>

                    )}
                </div>
                {(this.state.ourTurn || playerWin) ? (
                    <Dice onToss={playerDiced}
                          allowToss={!this.state.moving && this.state.ourTurn && !finished}
                    />
                ) : (
                    <RemoteDice onToss={enemyDiced}
                                allowToss={!this.state.moving && !this.state.ourTurn && !finished}
                                player={this.props.enemyPlayer}
                    />
                )}
            </div>

        );
    }
}

export default Game;
