import React, {Component} from 'react';
import DataTables from 'material-ui-datatables';
import {MuiThemeProvider} from "material-ui/styles/index";
import Parse from "parse";
import {LeaderBoardSize, MaxScore} from "./Consts";

const TABLE_COLUMNS = [
    {
        key: 'ranking',
        label: 'ranking',
    }, {
        key: 'username',
        label: 'username',
    }, {
        key: 'score',
        label: 'Score',
    }, {
        key: 'me',
        label: 'Where am I?',
    },
];


class LeaderBoard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tableData: [],
            totalPlayers: 0,
            worseThanMe: 0,
            myRanking: 0,
        };

        this.computeLeaderBoardTable = this.computeLeaderBoardTable.bind(this);
        this.updateTableData = this.updateTableData.bind(this);
        this.updateTotalPlayers = this.updateTotalPlayers.bind(this);
        this.updateWorseThanMe = this.updateWorseThanMe.bind(this);
        this.getLessThanOrEqual = this.getLessThanOrEqual.bind(this);
        this.findUserRanking = this.findUserRanking.bind(this);
    }

    updateWorseThanMe(worse) {
        if (this.state.worseThanMe !== worse)
            this.setState((prevState) => {
                return {worseThanMe: worse, myRanking: prevState.totalPlayers - worse}
            });
    }

    updateTotalPlayers(totalPlayers) {
        if (totalPlayers !== this.state.totalPlayers)
            this.setState((prevState) => {
                return {totalPlayers: totalPlayers, myRanking: totalPlayers - prevState.worseThanMe}
            });
    }

    getLessThanOrEqual(score, city, callback) {
        let listOfScores = [];
        let userScore = score;
        while(userScore > 0) {
            listOfScores.push(userScore);
            userScore -= (userScore & (-userScore));
        }

        let query = new Parse.Query('Score');
        query.equalTo("city", city);
        query.containedIn("score", listOfScores);
        query.find({
            success: function(results) {
                let res = 0;
                for (let i = 0; i < results.length; i++) {
                    let total = results[i].get("count");
                    res += total;
                }
                callback(res);
            },
            error: function(error) {
                alert(error);
            }
        });
    }

    updateTableData(tableData) {
        let changed = false;
        let prevState = this.state;

        if (prevState.tableData.length !== tableData.length) {
            changed = true;
        }
        else {
            for (let i = 0; i < prevState.tableData.length; i++) {
                if (prevState.tableData[i].score !== tableData[i].score ||
                    prevState.tableData[i].username !== tableData[i].username ||
                    prevState.tableData[i].ranking !== tableData[i].ranking) {
                    changed = true;
                    break;
                }
            }
        }

        if (changed)
            this.setState((prevState) => {
                    return {tableData: tableData};
            })
    }

    findUserRanking(user) {
        return {
            'score': user.get("score") * 50,
            'ranking': this.state.myRanking,
            'username': user.getUsername(),
            'me': 'here :D'
        };
    }

    computeLeaderBoardTable(user, city) {
        let tableData = [];
        let username = user.getUsername();

        let update = this.updateTableData;
        let findUserRanking = this.findUserRanking;
        let getLessThanOrEqual = this.getLessThanOrEqual;
        let updateTotalPlayers = this.updateTotalPlayers;
        let updateWorseThanMe = this.updateWorseThanMe;

        let query = new Parse.Query('LeaderBoard');
        query.equalTo("city", city);
        query.addDescending(['score', 'createdAt']);
        query.limit(LeaderBoardSize);
        query.find({
            success: function(results) {
                let userFounded = false;
                for (let i = 0; i < results.length; i++) {
                    let object = results[i];

                    tableData.push({
                        'score': object.get("score") * 50,
                        'username': object.get("username"),
                    });
                    if (object.get("username") === username) {
                        tableData[i].me = 'here :D';
                        userFounded = true;
                    }

                    for (let j = i - 1; j >= 0; j--) {
                        if (tableData[j + 1].score > tableData[j].score) {
                            let tmp = tableData[j + 1];
                            tableData[j + 1] = tableData[j];
                            tableData[j] = tmp;
                        }
                        else {
                            break;
                        }
                    }
                }
                for (let i = 0; i < tableData.length; i++)
                    tableData[i].ranking = i + 1;

                if (userFounded === false) {
                    getLessThanOrEqual(MaxScore, city, updateTotalPlayers);
                    getLessThanOrEqual(user.get("score"), city, updateWorseThanMe);

                    let temp = {
                        'score': "...",
                        'username': "...",
                        'ranking': "..."
                    };

                    tableData.push(temp);
                    tableData.push(findUserRanking(user, city));
                    tableData.push(temp);
                }

                update(tableData);
            },
            error: function(error) {
                alert(error);
            }
        });
    }

    render() {
        this.computeLeaderBoardTable(Parse.User.current(), this.props.city);
        return (
            <MuiThemeProvider>
                <DataTables
                    title="Leader Board"
                    showHeaderToolbarFilterIcon={false}
                    showHeaderToolbar={true}
                    columns={TABLE_COLUMNS}
                    data={this.state.tableData}
                    showFooterToolbar = {false}
                />
            </MuiThemeProvider>
        );
    }
}

export default LeaderBoard;
