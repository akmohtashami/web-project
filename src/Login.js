import React, { Component } from 'react';
import Parse from 'parse';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {Link} from 'react-router-dom'

class Login extends Component {

    constructor(props){
        super(props);
        this.state={
            username:'',
            password:''
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event){
        let username = this.state.username;
        let password = this.state.password;
        const callback = this.props.callback;

        Parse.User.logIn(username, password, {
            success: function(user) {
                callback();
            },
            error: function(user, error) {
                alert(error);
            }
        });
    }

    render() {
        return (
            <div>
                <div>
                    <AppBar
                        title="Login"
                    />
                    <TextField
                        hintText="Enter your Username"
                        floatingLabelText="Username"
                        onChange = {(event,newValue) => this.setState({username:newValue})}
                    />
                    <br/>
                    <TextField
                        type="password"
                        hintText="Enter your Password"
                        floatingLabelText="Password"
                        onChange = {(event,newValue) => this.setState({password:newValue})}
                    />
                    <br/>
                    <RaisedButton label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)}/>
                    <Link to="/register" >
                        <RaisedButton label="Register" primary={false} style={style} />
                    </Link>
                </div>
            </div>
        );
    }
}
const style = {
    margin: 15,
};

export default Login;