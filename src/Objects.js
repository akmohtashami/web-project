import * as Parse from "parse";
import {Coords} from "./Utils";

class GameDesc extends Parse.Object {
    constructor() {
        super("GameDesc");
    }
    static create(player1, player2) {
        let gameDesc = new GameDesc();
        gameDesc.set("player", [player1, player2]);
        gameDesc.set("turn", 0);
        gameDesc.set("coords", [new Coords(0, 0), new Coords(0, 0)]);
        return gameDesc
    }
}

Parse.Object.registerSubclass('GameDesc', GameDesc);

class GameRequest extends Parse.Object {
    constructor () {
        super("GameRequest");
    }
    static create(sender, receiver, game) {
        let gameRequest = new GameRequest();
        gameRequest.set("sender", sender);
        gameRequest.set("receiver", receiver);
        gameRequest.set("game", game);
        return gameRequest;
    }
}

Parse.Object.registerSubclass('GameRequest', GameRequest);

class GameDice extends Parse.Object {
    constructor () {
        super("GameDice");
    }
    static create(thrower, game, value) {
        let gameDice = new GameDice();
        gameDice.set("thrower", thrower);
        gameDice.set("game", game);
        gameDice.set("value", value);
        return gameDice;
    }
}

Parse.Object.registerSubclass('GameDice', GameDice);

export {GameDesc, GameRequest, GameDice};