/*
    {
        leaderboard: {city:None}
        dashboard: {game:None, initialized: false, started: false}
        game: {playerCoords, enemyCoords, moving, moveCounter, ourTurn}
        timer: {started, remaining}
    }
 */

export const INIT_DONE = 'INIT_DONE';
export const GAME_REQUESTED = 'GAME_REQUESTED';
export const GAME_CREATED = 'GAME_CREATED';
export const GAME_FINISHED = 'GAME_FINISHED';

export const DICE_THROWN = 'DICE_THROWN';
export const PLAYER_STEP = 'PLAYER_STEP';
export const PLAYER_SWITCH = 'PLAYER_SWITCH';

export const START_TIMER = 'START_TIMER';
export const TIMER_TICK = 'TIMER_TICK';
export const STOP_TIMER = 'TIMER_STOP';


export const CITY_FILTER = 'CITY_FILTER';
export const OPPONENT_FOUND = 'OPPONENT_FOUND';

export function init_done() {
    return {
        type: INIT_DONE,
    }
}

export function game_requested() {
    return {
        type: GAME_REQUESTED,
    }
}

export function game_finished(won) {
    return {
        type: GAME_FINISHED,
        won: won
    }
}

export function game_created() {
    return {
        type: GAME_CREATED,
    }
}

export function start_timer(time) {
    return {
        type: START_TIMER,
        time
    }
}

export function timer_tick() {
    return function (dispatch) {
        setTimeout(() => dispatch({
            type:TIMER_TICK
        }), 1000);
    }
}

export function stop_timer() {
    return {
        type: STOP_TIMER
    }
}

export function city_filter(city) {
    return {
        type: CITY_FILTER,
        city
    }
}

export function dice_thrown(num) {
    return {
        type: DICE_THROWN,
        num
    }
}

export function player_step() {
    return function(dispatch) {
        setTimeout(() => dispatch({
            type: PLAYER_STEP,
        }), 400);
    }
}

export function player_switch() {
    return {
        type: PLAYER_SWITCH
    }
}

export function opponent_found() {
    return {
        type: OPPONENT_FOUND
    }
}