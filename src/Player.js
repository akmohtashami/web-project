import React, { Component } from 'react';
import { Coords } from './Utils';
import {CellWidth, CellHeight} from "./Consts";

class Player extends Component {

    getRealCoords(coords) {
        return new Coords(
            coords.X * CellWidth,
            coords.Y * CellHeight
        )
    }


    render() {
        const coords = this.getRealCoords(this.props.coords);
        return (
            <img alt="player icon" src={this.props.icon}
                 className={
                     "playerIcon" +
                     (this.props.leftAligned ? " leftAligned": "") +
                     (this.props.rightAligned ? " rightAligned": "")
                 }
                 style={{
                     left: coords.X,
                     bottom: coords.Y,
                 }}
            />

        );
    }
}

export default Player;
