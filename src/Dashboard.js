import React, { Component } from 'react';
import Game from "./Game";
import {Link} from 'react-router-dom'
import Parse from 'parse'
import OpponentFinder from './OpponentFinder'
import RaisedButton from 'material-ui/RaisedButton';
import {GameDesc} from "./Objects";
import {game_requested, init_done} from "./actions";
import {connect} from "react-redux";

class BaseDashboard extends Component {
    constructor(props) {
        super(props);
        const user = Parse.User.current();
    }

    componentDidMount() {
        const user = Parse.User.current();
        user.set("game", null);
        user.save();
        this.cleanRequests();

    }

    cleanRequests() {
        Parse.LiveQuery.open();
        let onceOnly = false;
        Parse.LiveQuery.on("open", () => {
            if (onceOnly)
                return;
            const user = Parse.User.current();
            onceOnly = true;
            const query = new Parse.Query("GameRequest");
            query.equalTo("sender", user.get("username"));
            query.find().then((results) => {
                return Parse.Object.destroyAll(results);
            }).then(() => {
                this.props.dispatch(init_done());
            })
        });

    }


    render () {
        if (this.props.game) {
            const user = Parse.User.current();
            const ourIndex = this.props.game.get("player")[0] === user.get("username") ? 0 : 1;
            return (
                <div>
                    <p>{this.props.game.get("player")[0]} vs {this.props.game.get("player")[1] || 'BOT'}</p>
                    <Game playerCoords={this.props.game.get("coords")[ourIndex]}
                          enemyCoords={this.props.game.get("coords")[1 - ourIndex]}
                          ourTurn={this.props.game.get("turn") === ourIndex}
                          enemyPlayer={this.props.game.get("player")[1 - ourIndex]}
                    />
                    <Link to='/logout'>
                        <button>logout</button>
                    </Link>
                    <Link to='/leaderboard'>
                        <button>LeaderBoard</button>
                    </Link>
                </div>
            )
        }
        if (this.props.initializing)
            return (
                <div className="initializing">
                    Initializing...
                </div>
            );
        return (
            <div className="boardWrapper">
                <div className="board" style={{
                    opacity: 0.1
                }}>
                </div>
                <div className="startGame">
                    {this.props.started ? (
                        <OpponentFinder timeLimit={10}/>
                    ) : (
                        <RaisedButton label="Start!" primary={true} onClick={() => this.props.dispatch(game_requested())} />
                    )}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        game: state.dashboard.game,
        started: state.dashboard.started,
        initializing: !state.dashboard.initialized
    }
};

const mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
};

const Dashboard = connect(
    mapStateToProps,
    mapDispatchToProps,
)(BaseDashboard);

export default Dashboard;
