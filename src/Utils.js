import {BoardWidth, BoardHeight, LeaderBoardSize, MaxLog, MaxScore} from './Consts'
import Parse from 'parse'

function Coords(X, Y) {
    this.X = X;
    this.Y = Y;
}

function getDestCoords(coords, moveCnt) {
    let dx = null;
    if (coords.Y % 2 === 0)
        dx = 1;
    else
        dx = -1;
    let {X: nextX, Y: nextY} = coords;
    nextY += (moveCnt - moveCnt % BoardWidth) / BoardWidth;
    moveCnt = moveCnt % BoardWidth;
    if (nextX + dx * moveCnt < 0) {
        nextY++;
        nextX = moveCnt - nextX - 1;
    } else if (nextX + dx * moveCnt >= BoardWidth) {
        nextY++;
        nextX = BoardWidth - 1 - (moveCnt - (BoardWidth - 1 - nextX) - 1);
    }
    else
        nextX += dx * moveCnt;
    if (nextY >= BoardHeight) {
        nextY = BoardHeight - 1;
        if ((BoardHeight - 1) % 2 === 0)
            nextX = BoardWidth - 1;
        else
            nextX = 0;
    }
    return new Coords(nextX, nextY)
}

function addLeader(user, score, city) {
    let leaderBoard = Parse.Object.extend("LeaderBoard");
    leaderBoard = new leaderBoard();
    leaderBoard.set("score", score);
    leaderBoard.set("username", user.getUsername());
    leaderBoard.set("city", city);
    leaderBoard.save();
}

function updateLeaderBoard(user, score, city) {
    let query = new Parse.Query('LeaderBoard');
    query.equalTo("city", city);
    query.equalTo("username", user.getUsername());
    query.limit(1);
    query.find({
        success: function(results) {
            let added = false;
            for (let i = 0; i < results.length; i++) {
                if (results[i].get("username") === user.getUsername()) {
                    results[i].set("score", score);
                    results[i].save();
                    added = true;
                    break;
                }
            }
            if (!added) {
                addLeader(user, score, city);
            }
        },
        error: function(error) {
            alert(error);
        }
    });
}

function updateScores(score, addValue, city) {
    let listOfScores    = [];
    let startScore = score + 1;
    for(let i = 0; i < MaxLog; i++) {
        listOfScores.push(startScore);
        startScore += (startScore & (-startScore));
        if (startScore > MaxScore)
            break;
    }

    let query = new Parse.Query('Score');
    query.equalTo("city", city);
    query.containedIn("score", listOfScores);
    query.find({
        success: function(results) {
            for (let i = 0; i < results.length; i++) {
                if (addValue > 0)
                    results[i].increment("count", 1);
                else
                    results[i].increment("count", -1);
            }

            if (results)
                Parse.Object.saveAll(results, {});

            let newValues = [];
            for (let i = 0; i < listOfScores.length; i++) {
                let isInRes = false;
                for (let j = 0; j < results.length; j++) {
                    if (results[j].get("score") === listOfScores[i])
                        isInRes = true;
                }

                if (isInRes === false) {
                    let newObj = Parse.Object.extend("Score");
                    newObj = new newObj();
                    newObj.set("city", city);
                    newObj.set("score", listOfScores[i]);
                    newObj.set("count", addValue);
                    newValues.push(newObj);
                }
            }
            Parse.Object.saveAll(newValues, {});
        },
        error: function(error) {
            alert(error);
        }
    });
}

function newUser(city) {
    updateScores(0, 1, city);
    let user = Parse.User.current();
    updateLeaderBoard(user, user.get("score"), city);
}

function increaseScore(score) {
    const user = Parse.User.current();

    let currentScore = user.get("score");
    let city = user.get("city");

    user.increment("score", score);
    user.save();

    updateLeaderBoard(user, currentScore + score, "all");
    updateLeaderBoard(user, currentScore + score, city);

    updateScores(currentScore, -1, "all");
    updateScores(currentScore + score, 1, "all");
    updateScores(currentScore, -1, city);
    updateScores(currentScore + score, 1, city);
}

export {Coords, getDestCoords, newUser, increaseScore};