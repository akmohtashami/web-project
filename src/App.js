import React, { Component } from 'react';
import './App.css';
import Dashboard from './Dashboard';
import LeaderBoard from './LeaderBoard';
import Parse from 'parse';
import Login from "./Login";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Register from "./Register";

import {Route, Switch, BrowserRouter, Redirect} from 'react-router-dom'

Parse.initialize("myAppId", "jsKey");
Parse.serverURL = 'http://localhost:1337/parse';
Parse.LiveQuery.on('open', () => {
    console.log('socket connection established');
});
Parse.LiveQuery.on('error', (error) => {
    console.log(error);
});

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: null,
            isLoggedIn: !!Parse.User.current()
        };
        this.refreshAuthStatus = this.refreshAuthStatus.bind(this);
    }

    refreshAuthStatus() {
        this.setState({
            isLoggedIn: !!Parse.User.current()
        })
    }

    render() {
        return (
            <BrowserRouter>
                <MuiThemeProvider>
                    <div className={"App"}>

                    {!this.state.isLoggedIn ? (
                        <Switch>
                            <Route path="/login" render={() => <Login callback={this.refreshAuthStatus} />} />
                            <Route path="/register" render={() => <Register callback={this.refreshAuthStatus} />} />
                            <Route path="/" render={() => <Redirect to='/login' />} />
                        </Switch>
                    ) : (
                        <Switch>
                            <Route path="/logout" render={() => {
                                Parse.User.logOut().then(this.refreshAuthStatus);
                                return (<Redirect to='/' />);
                            }} />
                            <Route path="/dashboard" component={Dashboard} />
                            <Route path="/leaderboard" render={() => <LeaderBoard city="all" />} />
                            <Route path="/" render={() => <Redirect to='/dashboard' />} />
                        </Switch>
                    )}
                    </div>
                </MuiThemeProvider>
            </BrowserRouter>
        );
    }
}

export default App;
