import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Parse from "parse";
import {GameDesc, GameRequest} from "./Objects";
import {game_created, opponent_found, start_timer, stop_timer} from "./actions";
import { connect } from 'react-redux'

class BaseOpponentFinder extends Component {

    constructor(props) {
        super(props);
        this.subscription = null;
    }

    componentDidMount() {
        this.props.dispatch(start_timer(this.props.timeLimit));
        const user = Parse.User.current();

        const query1 = new Parse.Query("GameRequest");
        query1.equalTo("receiver", null);
        query1.find().then((results) => {
           if (results.length > 0) {
               this.props.dispatch(stop_timer());
               let req2 = GameDesc.create(user.get("username"), results[0].get("sender"));

               req2.save(null, {
                   success: (object) => {
                       const query = new Parse.Query("GameRequest");
                       query.equalTo("sender", results[0].get("sender"));
                       query.equalTo("receiver", user.get("username"));
                       this.subscription = query.subscribe();
                       this.subscription.on("create", (obj2) => {
                           user.set("game", object);
                           user.save(null, {
                               success: () => {
                                   this.props.callback()
                               },
                           });
                       });
                       let req = GameRequest.create(
                           user.get("username"),
                           results[0].get("sender"),
                           object
                       );
                       req.save(null, {
                           success: () => {
                               if (this.props.playerFound)
                                   return ;
                               this.props.dispatch(opponent_found());
                               setTimeout(() => this.props.callback(), this.props.timeLimit * 1000);
                           },
                           error: () => {
                               this.props.callback();
                           }
                       })
                   },
                   error: () => {
                       this.props.callback();
                   }
               })
           }
           else {

               let req = GameRequest.create(user.get("username"), null, null);
               const query = new Parse.Query("GameRequest");
               query.equalTo("receiver", user.get("username"));
               this.subscription = query.subscribe();

               this.subscription.on("create", (object) => {
                   let game = object.get("game");
                   game.fetch({
                       success: (game) => {
                           let req2 = GameRequest.create(
                               user.get("username"),
                               object.get("sender"),
                               game
                           );
                           req2.save(null, {
                               success: () => {
                                   this.props.dispatch(opponent_found());
                                   user.set("game", game);
                                   user.save(null, {
                                       success: () => {
                                           this.props.callback()
                                       },
                                   });
                               },
                               error: () => this.props.callback()
                           });

                       },
                       error: () => this.props.callback()
                   });


               });
               req.save(null, {
                   error: () => this.props.callback()
               });
           }
        });

    }

    componentWillUnmount(){
        if (this.subscription !== null)
            this.subscription.unsubscribe();
        this.props.dispatch(stop_timer())
    }

    countUp() {
        const counter = this.state.counter;
        if (counter < this.props.timeLimit)
            this.setState({
                counter: counter + 1
            });
        if (counter + 1 === this.props.timeLimit)
            this.props.callback();
        if (this.timer !== null)
            this.timer = setTimeout(this.countUp, 1000);
    }

    render() {
        return (
            <RaisedButton label={this.props.playerFound ?
                "Initializing the game..." :
                "Looking for opponent..." +
                (this.props.counter).toString()
            }  secondary={true} />
        )
    }
}

const mapStateToProps = state => {
    return {
        playerFound: !state.timer.started,
        counter: state.timer.remaining,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        callback: () => dispatch(game_created()),
        dispatch
    }
};

const OpponentFinder = connect(
    mapStateToProps,
    mapDispatchToProps,
)(BaseOpponentFinder);

export default OpponentFinder;
