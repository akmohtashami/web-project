import React, { Component } from 'react';
import Parse from 'parse';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {Link} from 'react-router-dom'
import {newUser} from "./Utils";


class Register extends Component {

    constructor(props){
        super(props);
        this.state={
            username: '',
            password: '',
            firstName: '',
            lastName: '',
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event){
        const callback = this.props.callback;

        let username = this.state.username;
        let password = this.state.password;
        let firstName = this.state.firstName;
        let lastName = this.state.lastName;

        let user = new Parse.User();
        user.set("username", username);
        user.set("password", password);
        user.set("firstName", firstName);
        user.set("lastName", lastName);
        user.set("city", "Tehran");
        user.set("score", 0);
        user.set("game", null);

        user.signUp(null, {
            success: function(user) {
                newUser(user.get("city"));
                newUser("all");
                callback();
            },
            error: function(user, error) {
                alert(error);
            }
        });
    }

    render() {
        return (
            <div>
                <div>
                    <AppBar
                        title="Register"
                    />
                    <TextField
                        hintText="Enter your First Name"
                        floatingLabelText="First Name"
                        onChange = {(event,newValue) => this.setState({firstName:newValue})}
                    />
                    <br/>
                    <TextField
                        hintText="Enter your Last Name"
                        floatingLabelText="Last Name"
                        onChange = {(event,newValue) => this.setState({lastName:newValue})}
                    />
                    <br/>
                    <TextField
                        hintText="Enter your Username"
                        floatingLabelText="Username"
                        onChange = {(event,newValue) => this.setState({username:newValue})}
                    />
                    <br/>
                    <TextField
                        type = "password"
                        hintText="Enter your Password"
                        floatingLabelText="Password"
                        onChange = {(event,newValue) => this.setState({password:newValue})}
                    />
                    <br/>
                    <RaisedButton label="Submit" primary={true} style={style} onClick={(event) => this.handleClick(event)}/>
                    <Link to="/login">
                        <RaisedButton label="Back" primary={false} style={style} />
                    </Link>
                </div>
            </div>
        );
    }
}

const style = {
    margin: 15,
};

export default Register;
