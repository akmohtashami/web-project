import React, { Component } from 'react';
import {GameDice} from "./Objects";
import * as Parse from "parse";



class Dice extends Component {

    constructor(props) {
        super(props);

        this.state = {
            allowToss: this.props.allowToss,
            diceText: "Throw!"
        };
        this.throwDice = this.throwDice.bind(this);
    }

    componentWillReceiveProps(props) {
        if (props.allowToss && !this.state.allowToss)
            this.setState({
                allowToss: props.allowToss,
                diceText: "Throw!"
            })
    }

    throwDice() {
        let result = Math.floor((Math.random() * 6) + 1);
        this.setState({
            allowToss: false,
            diceText: result.toString()
        });
        const user = Parse.User.current();
        let req = GameDice.create(user.get("username"), user.get("game"), result);
        req.save(null, {
            success: () => {this.props.onToss(result)}
        });
    }
    render() {
        return (
            <button onClick={this.throwDice} disabled={!this.state.allowToss}>
                {this.state.diceText}
            </button>
        )
    }
}

export default Dice;
