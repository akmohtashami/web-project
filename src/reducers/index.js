import { combineReducers } from 'redux'
import {timerReducer} from "./timer";
import {dashboardReducer} from "./dashboard";

const rootReducer = combineReducers({
    timer: timerReducer,
    dashboard: dashboardReducer
});

export default rootReducer;