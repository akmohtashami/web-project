import {GAME_CREATED, GAME_FINISHED, GAME_REQUESTED, INIT_DONE} from "../actions";
import * as Parse from "parse";
import {GameDesc} from "../Objects";

const initial_dashboard_state = {
    game: null,
    initialized: false,
    started: false,
    finished: false,
    won: false,
};

export function dashboardReducer(state = initial_dashboard_state, action) {
    switch (action.type) {
        case INIT_DONE:
            return {...state,
                initialized: true
            };
        case GAME_REQUESTED:
            return {...state,
                started: true
            };
        case GAME_FINISHED:
            return {...state,
                finished: true,
                won: action.won
            };
        case GAME_CREATED:
            if (state.game)
                return ;

            const user = Parse.User.current();
            let game = null;
            let excluded_user = null;
            if (user.get("game")) {
                excluded_user = user.get("game").get("player")[0] === user.get("username") ?
                    user.get("game").get("player")[1]:user.get("game").get("player")[0];
                game = user.get("game");
            } else {
                game = GameDesc.create(user.get("username"), null);
            }
            const query = new Parse.Query("GameRequest");
            query.equalTo("sender", user.get("username"));
            if (excluded_user)
                query.notEqualTo("receiver", excluded_user);
            query.find().then((results) => {
                return Parse.Object.destroyAll(results);
            });
            return {...state,
                game: game
            };
        default:
            return state;
    }
}

export default dashboardReducer;