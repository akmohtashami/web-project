import {game_created, START_TIMER, STOP_TIMER, TIMER_TICK, timer_tick} from "../actions";

const initial_timer_state = {
    started: false,
    remaining: 0,
};

export function timerReducer(state = initial_timer_state, action) {
    switch (action.type) {
        case START_TIMER:
            if (state.started)
                return state;
            action.asyncDispatch(timer_tick());
            return {
                ...state,
                started: true,
                remaining: action.time
            };
        case STOP_TIMER:
            return {
                ...state,
                started: false,
            };
        case TIMER_TICK:
            if (!state.started)
                return state;
            if (state.remaining === 0) {
                action.asyncDispatch(game_created());
                return {...state, started: false};
            }
            action.asyncDispatch(timer_tick());
            return {
                ...state,
                remaining: state.remaining - 1,
            };
        default:
            return state;
    }
}