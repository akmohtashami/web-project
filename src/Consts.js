let BoardWidth = 10;
let BoardHeight = 10;
let CellWidth = 95;
let CellHeight = 70;
const LeaderBoardSize = 20;
const MaxScore = 1e6;
const MaxLog = 20;

export {BoardWidth, BoardHeight, CellWidth, CellHeight, LeaderBoardSize, MaxScore, MaxLog};