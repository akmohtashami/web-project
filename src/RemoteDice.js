import React, { Component } from 'react';
import * as Parse from "parse";



class RemoteDice extends Component {

    constructor(props) {
        super(props);

        this.state = {
            allowToss: this.props.allowToss,
            diceText: "Throwing..."
        };
        this.subscription = null;
    }

    componentWillReceiveProps(props) {
        if (props.allowToss && !this.state.allowToss)
            this.setState({
                allowToss: props.allowToss,
                diceText: "Throwing..."
            })
    }

    diceThrown(num) {
        this.setState({
            allowToss: false,
            diceText: num.toString()
        });
        this.props.onToss(num);
    }

    tryRoll() {
        if (!this.state.allowToss)
            return ;
        if (this.props.player === null) {
            setTimeout(() => {
                const result = Math.floor((Math.random() * 6) + 1);
                this.diceThrown(result);
            }, 1000);
        }
        else {
            let query = new Parse.Query("GameDice");
            let user = Parse.User.current();
            query.equalTo("thrower", this.props.player);
            query.equalTo("game", user.get("game"));
            query.find((results) => {
                if (results.length > 0) {
                    results[0].destroy({
                        success: (obj) => this.diceThrown(obj.get("value"))
                    })
                }
                else
                    setTimeout(() => this.tryRoll(), 1000);
            });

        }
    }

    componentDidMount() {
        this.tryRoll()
    }

    componentDidUpdate(prevProps, prevState) {
        this.tryRoll();
    }

    componentWillUnmount() {
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.subscription = null;
        }
    }

    render() {
        return (
            <button disabled={true}>
                {this.state.diceText}
            </button>
        )
    }
}

export default RemoteDice;
